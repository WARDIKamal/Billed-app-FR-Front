import WindowIcon from "../assets/svg/window.js"
import MailIcon from "../assets/svg/mail.js"
import DisconnectIcon from "../assets/svg/disconnect.js"

export default (height) => {
  let user;
  user = JSON.parse(localStorage.getItem('user'))//parse() renvoie un objet
  if (typeof user === 'string') { 
    //ici on vérifie si user est un string sauf que le console.log annonce un objet
    user = JSON.parse(user)
  }

  if (user && user.type === 'Employee') {
    return (
      `
      <div class='vertical-navbar' style='height: ${height}vh;'>
        <div class='layout-title'> Billed </div>
        <div id='layout-icon1' data-testid="icon-window">
          ${WindowIcon}
        </div>
        <div id='layout-icon2' data-testid="icon-mail">
          ${MailIcon}
        </div>
        <div id='layout-disconnect' class="test">
          ${DisconnectIcon}
        </div>
    </div>
      `
    ) 
  } else {
    return (
      `
      <div class='vertical-navbar' style='height: ${height}vh;'>
        <div class='layout-title'> Billed </div>
          <div id='layout-disconnect' data-testid='layout-disconnect'>
            ${DisconnectIcon}
          </div>
        </div>
      `
    )
  }
}