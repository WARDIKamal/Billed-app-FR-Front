import { fireEvent, screen } from "@testing-library/dom";
import NewBillUI from "../views/NewBillUI.js";
import NewBill from "../containers/NewBill.js";
import { localStorageMock } from "../__mocks__/localStorage.js";
import { ROUTES_PATH } from "../constants/routes";
import Router from "../app/Router";
import store from "../__mocks__/store";
import BillsUI from "../views/BillsUI.js";


describe("Given I am connected as an employee", () => {
  beforeEach(() => {
    Object.defineProperty(window, "localStorage", { value: localStorageMock });
    window.localStorage.setItem('user', JSON.stringify({ type: 'Employee' }));

  });
  describe("When I am on NewBill Page", () => {
    test("Then email icon in vertical layout should be highlighted", () => {
      // Build DOM New Bill
      const pathNewBills = ROUTES_PATH["NewBill"];
      Object.defineProperty(window, "location", { value: { hash: pathNewBills } });
      document.body.innerHTML = `<div id="root"></div>`;
      Router();

      const iconMail = screen.getByTestId("icon-mail");
      expect(iconMail.classList.contains("active-icon")).toBe(true);
    });
    
    describe("When AUTHORIZED format file is uploaded", () => {
      test("Then the file name should be displayed into the input", () => {
        // Build DOM for new bill page
        const html = NewBillUI();
        document.body.innerHTML = html;

        // Mock function handleChangeFile()
        const onNavigate = (pathname) => {
          document.body.innerHTML = pathname
        };

        const thisNewBill = new NewBill({ document, onNavigate, store: null, localStorage });
        const mockChangeFileFn = jest.fn(thisNewBill.handleChangeFile);
        const file = screen.getByTestId("file");


        // Simulate if the file is an jpg extension
        file.addEventListener("change", mockChangeFileFn);
        fireEvent.change(file, {
          target: {
            files: [new File(["justif.jpg"], "justif.jpg", { type: "file/jpg" })],
          },
        });

        expect(mockChangeFileFn).toHaveBeenCalled();
        expect(file.files[0].name).toBe("justif.jpg");
      });
    })

    describe("When FORBIDEN format file is added", () => {
      test("Then the file should be reject by the app", () => {
        window.alert = jest.fn();
        const html = NewBillUI();
        document.body.innerHTML = html
        const store = null;
        const onNavigate = (pathname) => {
          document.body.innerHTML = pathname;
        };
        const thisNewBill = new NewBill({ document, onNavigate, store: null, localStorage });
        const mockChangeFileFn = jest.fn(() => thisNewBill.handleChangeFile);
        const file = screen.getByTestId("file");

        // Simulate wrong format
        file.addEventListener("change", mockChangeFileFn);
        fireEvent.change(file, {
          target: {
            files: [new File(["justif.pdf"], "justif.pdf", { type: "file/pdf" })],
          },
        });
        
        jest.spyOn(window, "alert");
        expect(alert).toHaveBeenCalled();
      });
    })
    

    describe("Given when click on submit button of form new bill", () => {
      test("Then should called handleSubmit function", () => {
        // Build DOM new bill
        const html = NewBillUI();
        document.body.innerHTML = html;

        // Mock function handleSubmit()
        const store = null;
        const onNavigate = (pathname) => {
          document.body.innerHTML = pathname;
        };

        const thisNewBill = new NewBill({ document, onNavigate, store, localStorage: window.localStorage });
        const submitFormNewBill = screen.getByTestId("form-new-bill");

        expect(submitFormNewBill).toBeTruthy();

        const mockHandleSubmit = jest.fn(thisNewBill.handleSubmit);
        submitFormNewBill.addEventListener("submit", mockHandleSubmit);
        fireEvent.submit(submitFormNewBill);

        expect(mockHandleSubmit).toHaveBeenCalled();
      });
      test("Then bill form is submited", () => {
        // Build Dom New bill
        const html = NewBillUI();
        document.body.innerHTML = html;

        // Mock function createBill()
        const store = null;
        const thisNewBill = new NewBill({ document, onNavigate, store, localStorage: window.localStorage });

        const mockCreateBillFn = jest.fn(thisNewBill.updateBill);
        const submitFormNewBill = screen.getByTestId("form-new-bill");

        submitFormNewBill.addEventListener("submit", mockCreateBillFn);
        fireEvent.submit(submitFormNewBill);

        expect(mockCreateBillFn).toHaveBeenCalled();
        expect(screen.getAllByText("Envoyer une note de frais")).toBeTruthy();
      });
    })
  })
})

//POST
describe("When I navigate to Dashboard employee", () => {
  test("Add a bill from mock API POST", async () => {
    const postSpy = jest.spyOn(store, "post");
    const newBill = {
      id: "47qAXb6fIm2zOKkLzMro",
      vat: "80",
      fileUrl:
        "https://test.storage.tld/v0/b/billable-677b6.a…f-1.jpg?alt=media&token=c1640e12-a24b-4b11-ae52-529112e9602a",
      status: "pending",
      type: "Hôtel et logement",
      commentary: "séminaire billed",
      name: "encore",
      fileName: "preview-facture-free-201801-pdf-1.jpg",
      date: "2004-04-04",
      amount: 400,
      commentAdmin: "ok",
      email: "a@a",
      pct: 20,
    };
    const bills = await store.post(newBill);
    expect(postSpy).toHaveBeenCalledTimes(1);
    expect(bills.data.length).toBe(5);
  })
  test("Add bills from an API and fails with 404 message error", async () => {
    store.post.mockImplementationOnce(() =>
      Promise.reject(new Error("Erreur 404"))
    );
    const html = BillsUI({ error: "Erreur 404" });
    document.body.innerHTML = html;
    const message = await screen.getByText(/Erreur 404/);
    expect(message).toBeTruthy();
  })
  test("Add bill from an API and fails with 500 message error", async () => {
    store.post.mockImplementationOnce(() =>
      Promise.reject(new Error("Erreur 500"))
    );
    const html = BillsUI({ error: "Erreur 500" });
    document.body.innerHTML = html;
    const message = await screen.getByText(/Erreur 500/);
    expect(message).toBeTruthy();
  })
})