import { screen } from '@testing-library/dom'
import userEvent from '@testing-library/user-event'
import BillsUI from '../views/BillsUI.js'
import Bills from '../containers/Bills.js'
import { bills } from '../fixtures/fixtureBills.js'
import { ROUTES, ROUTES_PATH } from '../constants/routes.js'
import Router from '../app/Router'

import { localStorageMock } from '../__mocks__/localStorage.js'
import store from '../__mocks__/store'

describe("Given I am connected as an employee", () => {
  beforeEach(() => {
    Object.defineProperty(window, "localStorage", { value: localStorageMock });
    window.localStorage.setItem('user', JSON.stringify({ type: 'Employee' }));
  });
  describe("When bills page is loaded", () => {
    test("Then title page and newbill button should be displayed", () => {
      const html = BillsUI({ data: [] });
      document.body.innerHTML = html;

      expect(screen.getAllByText("Mes notes de frais")).toBeTruthy();
      expect(screen.getByTestId("btn-new-bill")).toBeTruthy();
    });

    test("Then the window icon in vertical layout should be highlighted", () => {
      const pathname = ROUTES_PATH["Bills"]
      Object.defineProperty(window, "location", { value: { hash: pathname } })
      document.body.innerHTML = `<div id="root"></div>`
      Router()
      const iconWindow = screen.getByTestId("icon-window")
      expect(iconWindow.classList.contains("active-icon"))
    });
  });

  describe("When bills are loaded", () => {
    test("Then bill's data should be displayed", () => {
      const html = BillsUI({ data: bills });
      document.body.innerHTML = html;
      const billType = screen.getAllByTestId("type");
      const billName = screen.getAllByTestId("name");
      const billDate = screen.getAllByTestId("date");
      const billAmount = screen.getAllByTestId("amount");
      const billStatus = screen.getAllByTestId("status");
      const billIcon = screen.getAllByTestId("icon-eye");

      expect(billType.length).toBeGreaterThan(0);
      expect(billName.length).toBeGreaterThan(0);
      expect(billDate.length).toBeGreaterThan(0);
      expect(billAmount.length).toBeGreaterThan(0);
      expect(billStatus.length).toBeGreaterThan(0);
      expect(billIcon.length).toBeGreaterThan(0);
    });

    test("Then bills should be ordered from earliest to latest", () => {
      const html = BillsUI({ data: bills });
      document.body.innerHTML = html
      const dates = screen.getAllByText(/^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/i)
        .map(a => a.innerHTML)
      const antiChrono = (a, b) => ((a < b) ? 1 : -1)
      const datesSorted = [...dates].sort(antiChrono)
      expect(dates).toEqual(datesSorted)
    });
  });

  describe("When I click on newbill button", () => {
    test("The it should render NewBillUI", () => {
      const html = BillsUI({ data: bills });
      document.body.innerHTML = html;

      const store = null;
      const onNavigate = (pathname) => {
        document.body.innerHTML = ROUTES({ pathname });
      };
      const thisBill = new Bills({ document, onNavigate, store, localStorage: window.localStorage });
      const handleClickNewBill = jest.fn((e) => thisBill.handleClickNewBill());

      const btnNewBill = screen.getByTestId("btn-new-bill");
      btnNewBill.addEventListener('click', handleClickNewBill);
      userEvent.click(btnNewBill);

      expect(handleClickNewBill).toHaveBeenCalled();
      expect(screen.getByText("Envoyer une note de frais")).toBeTruthy();
    });
  });

  describe("When I click on the eye icon", () => {
    test('Then a modal should open', () => {
      const html = BillsUI({ data: bills });
      document.body.innerHTML = html;

      const store = null;
      const onNavigate = (pathname) => {
        document.body.innerHTML = ROUTES({ pathname });
      };
      const thisBill = new Bills({
        document,
        onNavigate,
        store,
        localStorage: window.localStorage
      });

      $.fn.modal = jest.fn();
      const eyes = screen.getAllByTestId("icon-eye");
      const mockClickIconEye = jest.fn(thisBill.handleClickIconEye(eyes[0]));

      eyes[0].addEventListener('click', mockClickIconEye);
      userEvent.click(eyes[0]);

      expect(eyes).toBeTruthy();
      expect(mockClickIconEye).toHaveBeenCalled();
    });
  });

  describe("When it's loading", () => {
    test('Then Loading page should be rendered', () => {
      const html = BillsUI({
        data: [],
        loading: true
      });
      document.body.innerHTML = html;
      expect(screen.getAllByText('Loading...')).toBeTruthy();
    });
  });

  describe("When there is an error", () => {
    test('Then Error page should be rendered', () => {
      const html = BillsUI({
        data: [],
        loading: false,
        error: 'some error message'
      });
      document.body.innerHTML = html;
      expect(screen.getAllByText('Erreur')).toBeTruthy();
    });
  });
});

// test d'intégration GET
describe("Given I am connected as an employee", () => {
  // GET integration test
  describe("When I navigate to Bills page", () => {
    test("fetches bills from mock API GET", async () => {
      const getSpy = jest.spyOn(store, "get")
      const bills = await store.get()
      expect(getSpy).toHaveBeenCalledTimes(1)
      expect(bills.data.length).toBe(4)
    });
    test("fetches bills from an API and fails with 404 message error", async () => {
      store.get.mockImplementationOnce(() =>
        Promise.reject(new Error("Erreur 404"))
      )
      const html = BillsUI({ error: "Erreur 404" })
      document.body.innerHTML = html
      const message = await screen.getByText(/Erreur 404/)
      expect(message).toBeTruthy()
    });
    test("fetches messages from an API and fails with 500 message error", async () => {
      store.get.mockImplementationOnce(() =>
        Promise.reject(new Error("Erreur 500"))
      )
      const html = BillsUI({ error: "Erreur 500" })
      document.body.innerHTML = html
      const message = await screen.getByText(/Erreur 500/)
      expect(message).toBeTruthy()
    });
  })
});