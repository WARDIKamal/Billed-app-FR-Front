import { formatDate } from '../app/format.js'
import DashboardFormUI from '../views/DashboardFormUI.js'
import BigBilledIcon from '../assets/svg/big_billed.js'
import { ROUTES_PATH } from '../constants/routes.js'
import USERS_TEST from '../constants/usersTest.js'
import Logout from "./Logout.js"

export const filteredBills = (data, status) => {
  return (data && data.length) ?
    data.filter(bill => {
      let selectCondition

      // in jest environment
      if (typeof jest !== 'undefined') {
        selectCondition = (bill.status === status)
      } else {
        // in prod environment
        const userEmail = JSON.parse(localStorage.getItem("user")).email
        selectCondition =
          (bill.status === status) &&
          ![...USERS_TEST, userEmail].includes(bill.email)
      }

      return selectCondition
    }) : []
}

/**
 * @description - Build the card DOM element
 * @param {*} bill - bill object
 * @returns - DOM element with bill information
 */
export const card = (bill) => {
  const firstAndLastNames = bill.email.split('@')[0]
  const firstName = firstAndLastNames.includes('.') ?
    firstAndLastNames.split('.')[0] : ''
  const lastName = firstAndLastNames.includes('.') ?
  firstAndLastNames.split('.')[1] : firstAndLastNames

  return (`
    <div class='bill-card' id='open-bill${bill.id}' data-testid='open-bill${bill.id}'>
      <div class='bill-card-name-container'>
        <div class='bill-card-name'> ${firstName} ${lastName} </div>
        <span class='bill-card-grey'> ... </span>
      </div>
      <div class='name-price-container'>
        <span> ${bill.name} </span>
        <span> ${bill.amount} € </span>
      </div>
      <div class='date-type-container'>
        <span> ${formatDate(bill.date)} </span>
        <span> ${bill.type} </span>
      </div>
    </div>
  `)
}

/**
 * @description - Build the dashboard UI DOM
 * @param {*} bills - bills array
 * @returns - DOM element with bills information or empty string
 */
export const cards = (bills) => {
  return bills && bills.length ? bills.map(bill => card(bill)).join("") : ""
}

/**
 * @description - get the bill status
 * @param {*} index - bill index
 */
export const getStatus = (index) => {
  switch (index) {
    case 1:
      return "pending"
    case 2:
      return "accepted"
    case 3:
      return "refused"
  }
}

/**
 * @description - Class for the dashboard UI with eventlisteners of lists, cards and modal
 */
export default class {
  constructor({ document, onNavigate, store, bills, localStorage }) {
    this.document = document
    this.onNavigate = onNavigate
    this.store = store
    $('#arrow-icon1').on("click", (e) => this.handleShowTickets(e, bills, 1))
    $('#arrow-icon2').on("click", (e) => this.handleShowTickets(e, bills, 2))
    $('#arrow-icon3').on("click", (e) => this.handleShowTickets(e, bills, 3))
    this.getBillsAllUsers()
    new Logout({ localStorage, onNavigate })
  }

  /**
   * @description - handle the click event on the eye icon and open the modal
   */
  handleClickIconEye = () => {
    const billUrl = $('#icon-eye-d').attr("data-bill-url")
    const imgWidth = Math.floor($('#modaleFileAdmin1').width() * 0.8)
    $('#modaleFileAdmin1').find(".modal-body").html(`<div style='text-align: center;'><img width=${imgWidth} src=${billUrl} /></div>`)
    if (typeof $('#modaleFileAdmin1').modal === 'function') $('#modaleFileAdmin1').modal('show')
  }
  
  /**
   * @description - handle the click on ticket
   * @param {*} e - event
   * @param {*} bill - the bill clicked
   * @param {*} bills - all the bills
   */
  handleEditTicket(e, bill, bills) {
    e.stopImmediatePropagation() // stop bubbling (i prefer to use this)
    if (this.counter === undefined || this.id !== bill.id) this.counter = 0
    if (this.id === undefined || this.id !== bill.id) this.id = bill.id
    if (this.counter % 2 === 0) {

        bills.forEach((b) => {
          $(`#open-bill${b.id}`).css({ background: "#0D5AE5" })
        });
        $(`#open-bill${bill.id}`).css({ background: "#2A2B35" })
        $(".dashboard-right-container div").html(DashboardFormUI(bill))
        $(".vertical-navbar").css({ height: "150vh" })
        this.counter++
      } else {
        $(`#open-bill${bill.id}`).css({ background: "#0D5AE5" })

        $(".dashboard-right-container div").html(`
        <div id="big-billed-icon"> ${BigBilledIcon} </div>
      `)
        $(".vertical-navbar").css({ height: "120vh" })
        this.counter++
      }
    $("#icon-eye-d").on('click', this.handleClickIconEye)
    $("#btn-accept-bill").on('click', (e) => this.handleAcceptSubmit(e, bill))
    $("#btn-refuse-bill").on('click', (e) => this.handleRefuseSubmit(e, bill))
  }

  /**
   * @description - handle the click on submit button to accept a bill
   * @param {*} e - event
   * @param {*} bill - the bill clicked 
   */
  handleAcceptSubmit = (e, bill) => {
    const newBill = {
      ...bill,
      status: 'accepted',
      commentAdmin: $('#commentary2').val()
    }
    this.updateBill(newBill)
    this.onNavigate(ROUTES_PATH['Dashboard'])
  }
  /**
   * @description - handle the click on submit button to refuse a bill
   * @param {*} e - event
   * @param {*} bill - the bill clicked 
   */
  handleRefuseSubmit = (e, bill) => {
    const newBill = {
      ...bill,
      status: 'refused',
      commentAdmin: $('#commentary2').val()
    }
    this.updateBill(newBill)
    this.onNavigate(ROUTES_PATH['Dashboard'])
  }
  /**
   * @description - show list of tickets
   * @param {*} bills - all the bills
   * @param {*} index - the index of the status
   */
  showList(bills, index) {
    this.index = index
    $(`#arrow-icon${this.index}`).css({ transform: "rotate(0deg)" })
        $(`#status-bills-container${this.index}`)
        .html(cards(filteredBills(bills, getStatus(this.index))))
  }
  /**
   * @description - hide list of tickets
   * @param {*} index - the index of the status
   */
  hideList(index) {
    this.index = index
    $(`#arrow-icon${this.index}`).css({ transform: "rotate(90deg)" })
        $(`#status-bills-container${this.index}`).html("")
  }

  /**
   * @description - handle the click on ticket
   * @param {*} e - event
   * @param {*} bills - all the bills
   * @param {*} index - the index of the status
   * @returns {*} - the bills
   */
  handleShowTickets(e, bills, index) {
    if (this.listCounter === undefined || this.index !== index) this.listCounter = 0
    if (this.index === undefined || this.index !== index) this.index = index
      if (this.listCounter % 2 === 0) {
      this.showList(bills, this.index)
      this.listCounter++
    } else {
      this.hideList(this.index)
      this.listCounter++
    }
    bills.forEach((bill) => {
      // $(`#open-bill${bill.id}`).off() //remove event listener for each bill (like a reset)
      $(`#open-bill${bill.id}`).on('click',(e) => this.handleEditTicket(e, bill, bills));
    })
    return bills
  }
  
  // not need to cover this function by tests
  /* istanbul ignore next */
  /**
   * @description - get all the bills of all users from database
   * @returns {*} - the bills
   */
  getBillsAllUsers = () => {
    if (this.store) {
      return this.store
      .bills()
      .list()
      .then(snapshot => {
        const bills = snapshot
        .map(doc => ({
          id: doc.id,
          ...doc,
          date: doc.date,
          status: doc.status
        }))
        return bills
      })
      .catch(console.log)
    }
  }
  // not need to cover this function by tests 
  /* istanbul ignore next */
  /**
   * @description - update a bill in database
   * @param {*} bill - the bill to update
   * @returns - the bill updated
   */
  updateBill = (bill) => {
    if (this.store) {
    return this.store
      .bills()
      .update({data: JSON.stringify(bill), selector: bill.id})
      .then(bill => bill)
      .catch(console.log)
    }
  }
}