
## L'architecture du projet :
Ce projet, dit frontend, est connecté à un service API backend que vous devez aussi lancer en local.

Le projet backend se trouve ici: https://github.com/WARDI-Kamal/Billed-app-FR-Back.git

## Organiser son espace de travail :
Pour une bonne organization, vous pouvez créer un dossier bill-app dans lequel vous allez cloner le projet backend et par la suite, le projet frontend:

Clonez le projet backend dans le dossier bill-app :
```
$ git clone https://github.com/OpenClassrooms-Student-Center/Billed-app-FR-Back.git
```

```
bill-app/
   - Billed-app-FR-Back
```

Clonez le projet frontend dans le dossier bill-app :
```
$ git clone https://github.com/OpenClassrooms-Student-Center/Billed-app-FR-Front.git
```

```
bill-app/
   - Billed-app-FR-Back
   - Billed-app-FR-Front
```

================ BACK ====================

### Acceder au repertoire du projet :
```
cd Billed-app-FR-Back
```

### Installer les dépendances du projet :

```
npm install
```

================ FRONT ====================

### Acceder au repertoire du projet :
```
$ cd Billed-app-FR-Front
```

### Installez les packages npm (décrits dans `package.json`) :

```
$ npm install
```

### Installez live-server pour lancer un serveur local :
```
$ npm install -g live-server
```

# Comment lancer l'application en local ?

### étape 1 - Lancer l'API backend :

```
$ cd Billed-app-FR-Back
npm run node server.js
```

### étape 2 - Lancer le frontend :

Lancez live-server :
```
$ cd Billed-app-FR-Front
$ live-server
```

### étape 3 - Accéder à 
```
http://localhost:5500/
```

## Comment lancer tous les tests en local avec Jest ?

```
$ npm run test
```

## Comment lancer un seul test ?

```
$npm run test your_test_file.js
```

## Comment voir la couverture de test ?

`http://localhost:5500/coverage/lcov-report/`

## Comptes et utilisateurs :

Vous pouvez vous connecter en utilisant les comptes:

### administrateur : 
```
utilisateur : admin@test.tld 
mot de passe : admin
```
### employé :
```
utilisateur : employee@test.tld
mot de passe : employee
```
---
[![wakatime](https://wakatime.com/badge/user/e9b04158-0482-414a-b06c-6cc1f928b1c7/project/353da8ed-fa46-4c0c-950f-272b5b214cbf.svg)](https://wakatime.com/badge/user/e9b04158-0482-414a-b06c-6cc1f928b1c7/project/353da8ed-fa46-4c0c-950f-272b5b214cbf)
